# algoritmo que calcula el area de un triangulo

base_triangulo = 5
altura_triangulo = 6

# calculando la operacion
area_triangulo = base_triangulo * altura_triangulo / 2

print("el area del triangulo es: ", area_triangulo)

# --------------------------//------------------------------------------------

# algoritmo que determina si un numero es par o no

numero_ingresado = int(input("Digite el numero a validar: "))

# validando el numero ingresado
if (numero_ingresado % 2 == 0):
    print("El numero ingresado es par")
else:
    print("El numero no es par")

# ---------------------------------//------------------------------------------

# algoritmo que calcula la suma de los enteros desde 1 hasta n

entero_positivo = int(input("Digite un numero: "))

# validando si el numero es positivo
if entero_positivo > 0:
    # realizando la sumatoria si el entero es positivo
    valor_sumatoria = entero_positivo * (entero_positivo + 1) / 2

print(int(valor_sumatoria))
